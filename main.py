import cache as c

if __name__ == '__main__':

    cache = c.Cache()
    cache.retrieve('https://www.python.org/')
    cache.show('https://www.python.org/')
    cache.retrieve("https://www.aulavirtual.urjc.es/moodle/")
    cache.show_all()
