import urllib.request


class Robot:
    def __init__(self, url):
        self.url = url
        self.data = None
        self.retrieved = False

    def retrieve(self):
        if not self.retrieved:
            print('Descargando url')
            file, headers = urllib.request.urlretrieve(self.url)
            with open(file, 'r') as f:
                self.data = f.read()
            self.retrieved = True

    def show(self):
        if self.data is None:
            print("No se ha descargado nada")
            return
        print(self.data)

    def content(self):
        cad = self.data
        return cad